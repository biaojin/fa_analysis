# -*- coding: utf-8 -*-
# 京东
# 解析


import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import BaseClass, DemoCls


# class JingDong(DemoCls):
class JingDong(BaseClass):

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        num01 = 0
        for i in data:
            if len(i.get('s3_img', '')) > 1 and i.get('list_data', {}).get('data_json', {}).get('title', '') and i.get('url') and i.get('version'):
                # if i.get('push_state') == '1':
                #     print('数据以推送')
                #     continue
                if self.fa_data(i):
                    num += 1
            else:
                self.write_error_text(fa_name=i.get('fa_name', ''), e='11111', text='没有图片 {}'.format(i.get('id', '')))
        print(22222, num)
        return num, num01

    def mongo_data(self, version):
        try:
            print(version)
            m_data = self.mongo.h_spider.spider_data.find(version)
            return m_data
        except Exception as e:
            print(traceback.print_exc())

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            t_tags = data.get('list_data', {}).get('data_json', {}).get('tags', '')
            item_mongo['t_tags'] = ' '.join(t_tags) if t_tags and len(t_tags) > 0 else ''

            item_mongo['tags'] = item_mongo['t_tags'][3]
            item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('price', '')
            item_mongo['f_rate'] = data.get('list_data', {}).get('data_json', {}).get('good', '')
            item_mongo['store_name'] = data.get('list_data', {}).get('data_json', {}).get('shop_name', '')

            item_mongo['c_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')

            item_mongo['url'] = data.get('url', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['rank'] = data.get('rank', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['img'] = data.get('s3_img', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['source'] = 'path'
            item_mongo['product_parm'] = ''
            item_mongo['discount'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['province'] = ''
            item_mongo['city'] = ''
            item_mongo['area'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
            # import json
            # with open('./京东.txt', 'a+', encoding='utf-8') as f:
            #     f.write(json.dumps(item_mongo, ensure_ascii=False))
            #     f.write('\n')
        except Exception as e:
            print(traceback.print_exc())


if __name__ == '__main__':
    con = 0
    con01 = 0
    ddky = JingDong()
    data_time = [
        # {"crawlstamp": {'$gte': 1629941078, '$lte': 1630056278}, "version": "20210901-jingdong"},
        # {"crawlstamp": {'$gte': 1630380278, '$lte': 1630423478}, "version": "20210901-jingdong"},
        # {"crawlstamp": {'$gte': 1630423478, '$lte': 1630466678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630466678, '$lte': 1630502678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630538678, '$lte': 1630574678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630574678, '$lte': 1630610678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630610678, '$lte': 1630646678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630646678, '$lte': 1630682678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630682678, '$lte': 1630718678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630718678, '$lte': 1630754678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630754678, '$lte': 1630790678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630790678, '$lte': 1630826678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630826678, '$lte': 1630862678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630862678, '$lte': 1630898678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630898678, '$lte': 1630934678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630934678, '$lte': 1630970678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1630970678, '$lte': 1631006678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631006678, '$lte': 1631042678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631042678, '$lte': 1631078678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631078678, '$lte': 1631114678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631150678, '$lte': 1631186678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631186678, '$lte': 1631222678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631222678, '$lte': 1631258678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631258678, '$lte': 1631294678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631294678, '$lte': 1631330678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631330678, '$lte': 1631366678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631366678, '$lte': 1631402678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631402678, '$lte': 1631438678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631438678, '$lte': 1631474678}, "version": "20210901-jingdong"},
        {"crawlstamp": {'$gte': 1631474678, '$lte': 1631676278}, "version": "20210901-jingdong"}]
    for i in data_time:
        num, num01 = ddky.run(i)
        con += num
        con01 += num01
    print('全部数据  {}'.format(con01))
    print('最后成功  {}'.format(con))
    ddky.close()


    # ddky.run({"crawlstamp": {'$gte': 1630682678, '$lte': 1630718678}, "version": "20210901-jingdong"})
    # ddky.close()
    # print(ddky.mongo.h_spider.spider_data.find_one({'version': '%s' % version}))
