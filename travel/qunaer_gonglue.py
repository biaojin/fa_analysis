# coding=utf-8
# coding=utf-8
import sys

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')
from base_class import BaseClass
import traceback


class Iqiyi_push(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['trave_time'] = data.get('detail-data', {}).get('api_json', {}).get('date', '').replace('出发', '')
            item_mongo['tags'] = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            item_mongo['traveler'] = data.get('detail-data', {}).get('api_json', {}).get('character', '')
            item_mongo['price'] = data.get('detail-data', {}).get('api_json', {}).get('per_capita', '')
            item_mongo['buy_desc'] = data.get('detail-data', {}).get('api_json', {}).get('subtitle', '')
            item_mongo['p_count'] = data.get('detail-data', {}).get('api_json', {}).get('comment', '').replace('评论',
                                                                                                               '').replace(
                '（', '').replace('）', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['city'] = data.get('path_label', '')[-2]
            item_mongo['img'] = data.get('s3_img', '')
            # item_mongo['img_up'] = data.get('detail-data', {}).get('api_json', {}).get('img', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['charact'] = ''
            item_mongo['score'] = ''
            item_mongo['booking_desc'] = ''
            item_mongo['travel_days'] = ''
            item_mongo['go_way'] = ''
            item_mongo['cost_desc'] = ''
            item_mongo['sales_count'] = ''
            item_mongo['end_area'] = ''
            item_mongo['attract_name'] = ''
            item_mongo['s_tag'] = ''
            item_mongo['play_way'] = ''
            item_mongo['safe_tag'] = ''
            item_mongo['Itinerary'] = ''
            item_mongo['origin_area'] = ''
            item_mongo['scenic_rate'] = ''
            item_mongo['scenic_desc'] = ''
            item_mongo['province'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['source'] = 'path'

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def run(self, version):
        num = 0
        data = self.mongo_data(version)

        for i in data:
            if len(i.get('s3_img', '')) > 0:
                # if i.get('push_state') == '1' or i.get('channel') == '度假':
                if i.get('channel') == '度假':
                    print('数据以推送')
                    continue
                if self.tengxun_data(i):
                    num += 1
        print(num)


if __name__ == '__main__':
    version = '20210802-com-qunar'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
