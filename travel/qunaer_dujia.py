# coding=utf-8
# coding=utf-8
import sys

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')
from base_class import BaseClass
import traceback


class Iqiyi_push(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')

        try:
            item_mongo = dict()

            item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('accuratePrice', '')
            item_mongo['score'] = data.get('list_data', {}).get('data_json', {}).get('score', '')
            item_mongo['p_count'] = data.get('list_data', {}).get('data_json', {}).get('reviews', '')
            item_mongo['city'] = data.get('list_data', {}).get('data_json', {}).get('arrives', '')
            item_mongo['travel_days'] = data.get('list_data', {}).get('data_json', {}).get('hotSpringSubTitle', '')
            item_mongo['sales_count'] = data.get('list_data', {}).get('data_json', {}).get('soldcount', '')
            attract_name = data.get('list_data', {}).get('data_json', {}).get('detailSights', '')
            item_mongo['attract_name'] = ','.join(attract_name) if attract_name and len(attract_name) > 0 else ''

            feature = data.get('detail-data', {}).get('api_json', {}).get('feature', '')
            attention = data.get('detail-data', {}).get('api_json', {}).get('attention', '')
            tip = data.get('detail-data', {}).get('api_json', {}).get('tip', '')
            refundDesc = data.get('detail-data', {}).get('api_json', {}).get('refundDesc', '')
            item_mongo['buy_desc'] = feature + ' ' + attention + ' ' + tip + ' ' + refundDesc
            tags = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            item_mongo['tags'] = ','.join(tags) if tags and len(tags) > 0 else ''
            item_mongo['area'] = data.get('detail-data', {}).get('api_json', {}).get('city', '')
            item_mongo['end_area'] = data.get('detail-data', {}).get('api_json', {}).get('city', '')
            item_mongo['cost_desc'] = data.get('detail-data', {}).get('api_json', {}).get('feeAllEntries', '')
            item_mongo['go_way'] = data.get('detail-data', {}).get('api_json', {}).get('totraffic', '')
            s_tag = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            item_mongo['s_tag'] = ','.join(s_tag) if s_tag and len(s_tag) > 0 else ''
            item_mongo['booking_desc'] = feature + '。 ' + attention + '。 ' + tip + '。charact ' + refundDesc
            item_mongo['charact'] = data.get('detail-data', {}).get('api_json', {}).get('feature', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['img'] = data.get('s3_img', '')
            # item_mongo['img_up'] = data.get('list_data', {}).get('data_json', {}).get('imgs', '')

            dep = data.get('list_data', {}).get('data_json', {}).get('dep', '')
            arrives = data.get('list_data', {}).get('data_json', {}).get('arrives', '')
            title = data.get('list_data', {}).get('data_json', {}).get('title', '')
            if dep and arrives and title:
                item_mongo['title'] = '{}->{} {}'.format(dep, arrives, title)
            elif dep and title:
                item_mongo['title'] = '{} {}'.format(dep, title)
            else:
                item_mongo['title'] = title

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['play_way'] = ''
            item_mongo['safe_tag'] = ''
            item_mongo['Itinerary'] = ''
            item_mongo['origin_area'] = ''
            item_mongo['scenic_rate'] = ''
            item_mongo['scenic_desc'] = ''
            item_mongo['trave_time'] = ''
            item_mongo['traveler'] = ''
            item_mongo['province'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['source'] = 'path'

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print('解析报错', e)
            print('解析报错', traceback.print_exc())
            # print('解析报错', self.redis.rpush('qunaer_error', str(data.get('path_label'))))

    def run(self, version):
        num = 0
        try:
            data = self.mongo_data(version)
            for i in data:
                if len(i.get('s3_img', '')) > 0:
                    # if i.get('push_state') == '1' or i.get('channel') == '攻略':
                    if i.get('channel') == '攻略':
                        print('数据以推送')
                        continue
                    if self.tengxun_data(i):
                        num += 1
                    else:
                        print('失败')
        except Exception as e:
            print('获取数据报错', e)
        print(num)


if __name__ == '__main__':
    version = '20210802-com-qunar'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
