# coding=utf-8
import re

import pymongo
import time
import traceback
from kafka import KafkaProducer
import json

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


# class TengXun(DemoCls):
class TengXun(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['area'] = data.get('list_data', {}).get('data_json', {}).get('address', '').replace('地址：', '')
            item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('img', '')
            item_mongo['attract_name'] = data.get('list_data', {}).get('data_json', {}).get('address', '')
            item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('price', '')
            item_mongo['scenic_rate'] = data.get('list_data', {}).get('data_json', {}).get('level', '')
            item_mongo['score'] = data.get('list_data', {}).get('data_json', {}).get('score', '')
            item_mongo['p_count'] = data.get('list_data', {}).get('data_json', {}).get('c_count', '')
            item_mongo['charact'] = data.get('list_data', {}).get('data_json', {}).get('subtitle', '')

            introduce = data.get('detail-data', {}).get('api_json', {}).get('introduce', '')
            opening_hours = data.get('detail-data', {}).get('api_json', {}).get('opening_hours', '')
            policy = data.get('detail-data', {}).get('api_json', {}).get('policy', '')
            notice = data.get('detail-data', {}).get('api_json', {}).get('notice', '')
            item_mongo['buy_desc'] = introduce + '。 ' + opening_hours + '。 ' + policy + '。 ' + notice
            item_mongo['travel_days'] = data.get('detail-data', {}).get('api_json', {}).get('notice', '')
            item_mongo['scenic_desc'] = data.get('detail-data', {}).get('api_json', {}).get('introduce', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['city'] = data.get('path_label', '')[-1]

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['play_way'] = ''
            item_mongo['safe_tag'] = ''
            item_mongo['booking_desc'] = ''
            item_mongo['go_way'] = ''
            item_mongo['cost_desc'] = ''
            item_mongo['Itinerary'] = ''
            item_mongo['end_area'] = ''
            item_mongo['origin_area'] = ''
            item_mongo['sales_count'] = ''
            item_mongo['trave_time'] = ''
            item_mongo['traveler'] = ''
            item_mongo['s_tag'] = ''
            item_mongo['province'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['source'] = 'path'

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if self.fa_data(i):
                    num += 1
            else:
                print('图片没有刷新', i.get('id'))
        print(num)


if __name__ == '__main__':
    version = '20210804-com-tongcheng'
    ddky = TengXun()
    ddky.run(version)
    # ddky.check_mongo_data(version)
    ddky.close()

