# coding=utf-8

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


# class TengXun(DemoCls):
class TengXun(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['city'] = data.get('list_data', {}).get('data_json', {}).get('place', '')
            item_mongo['end_area'] = data.get('list_data', {}).get('data_json', {}).get('place', '')
            item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('imgs', '')
            item_mongo['trave_time'] = data.get('list_data', {}).get('data_json', {}).get('departuredate', '')
            item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('consume', '')
            item_mongo['travel_days'] = data.get('list_data', {}).get('data_json', {}).get('traveldays', '')
            item_mongo['p_count'] = data.get('list_data', {}).get('data_json', {}).get('v_count', '')

            item_mongo['tags'] = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            item_mongo['date_str'] = data.get('detail-data', {}).get('api_json', {}).get('publishTime', '')
            item_mongo['traveler'] = data.get('detail-data', {}).get('api_json', {}).get('companion', '')
            item_mongo['buy_desc'] = data.get('detail-data', {}).get('api_json', {}).get('content', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['vers'] = data.get('version', '')

            item_mongo['source'] = 'path'
            item_mongo['charact'] = ''
            item_mongo['play_way'] = ''
            item_mongo['safe_tag'] = ''
            item_mongo['score'] = ''
            item_mongo['scenic_desc'] = ''
            item_mongo['scenic_rate'] = ''
            item_mongo['booking_desc'] = ''
            item_mongo['go_way'] = ''
            item_mongo['cost_desc'] = ''
            item_mongo['Itinerary'] = ''
            item_mongo['origin_area'] = ''
            item_mongo['s_tag'] = ''
            item_mongo['attract_name'] = ''
            item_mongo['area'] = ''
            item_mongo['province'] = ''
            item_mongo['intention'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                # if i.get('push_state') == '1' or i.get('channel') == '旅游':
                if i.get('channel') == '旅游':
                    print('数据以推送')
                    continue
                if self.fa_data(i):
                    num += 1
            else:
                # self.write_error_text(i.get('fa_name', ''), e='', text='')
                print('图片没有s3_img刷新', i.get('id'))
        print(num)


if __name__ == '__main__':
    version = '20200805-ctrip-android-view'
    ddky = TengXun()
    ddky.run(version)
    # ddky.check_mongo_data(version)
    ddky.close()
