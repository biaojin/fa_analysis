# coding=utf-8



import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


# class TengXun(DemoCls):
class TengXun(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['city'] = data.get('detail-data', {}).get('api_json', {}).get('DestCityName', '')
            item_mongo['price'] = data.get('detail-data', {}).get('api_json', {}).get('price', '')
            item_mongo['origin_area'] = data.get('detail-data', {}).get('api_json', {}).get('DepartureCityName', '')
            item_mongo['end_area'] = data.get('detail-data', {}).get('api_json', {}).get('DestCityName', '')
            item_mongo['Itinerary'] = data.get('detail-data', {}).get('api_json', {}).get('xing_cheng', '')
            item_mongo['cost_desc'] = data.get('detail-data', {}).get('api_json', {}).get('CostInfoList', '')
            item_mongo['sales_count'] = data.get('detail-data', {}).get('api_json', {}).get('sales_count', '')
            item_mongo['travel_days'] = data.get('detail-data', {}).get('api_json', {}).get('TravelDays', '')
            item_mongo['booking_desc'] = data.get('detail-data', {}).get('api_json', {}).get('booking_desc', '')
            item_mongo['score'] = data.get('detail-data', {}).get('api_json', {}).get('score', '')
            item_mongo['p_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')
            item_mongo['img'] = data.get('detail-data', {}).get('api_json', {}).get('imgs', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['vers'] = data.get('version', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['source'] = 'path'
            item_mongo['title_keyword'] = ''
            item_mongo['scenic_desc'] = ''
            item_mongo['scenic_rate'] = ''
            item_mongo['trave_time'] = ''
            item_mongo['intention'] = ''
            item_mongo['play_way'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['safe_tag'] = ''
            item_mongo['buy_desc'] = ''
            item_mongo['date_str'] = ''
            item_mongo['province'] = ''
            item_mongo['traveler'] = ''
            item_mongo['charact'] = ''
            item_mongo['go_way'] = ''
            item_mongo['s_tag'] = ''
            item_mongo['type'] = ''
            item_mongo['tags'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            pass
            # self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                # if i.get('push_state') == '1' or i.get('channel') == '攻略/景点':
                if i.get('channel') == '攻略/景点':
                    print('数据以推送')
                    continue
                if self.fa_data(i):
                    num += 1
            else:
                print('图片没有刷新', i.get('id'))
        print(num)


if __name__ == '__main__':
    version = '20200805-ctrip-android-view'
    ddky = TengXun()
    ddky.run(version)
    # ddky.check_mongo_data(version)
    ddky.close()

    # print(ddky.mongo.h_spider.spider_data.find_one({'version': '%s' % version}))

