# coding=utf-8
import re
import time
import pymongo
import time
import traceback
from kafka import KafkaProducer
import json


def stamp_to_date(stamp):
    timeArray = time.localtime(stamp)
    otherStyleTime = time.strftime("%Y-%m-%d", timeArray)
    return otherStyleTime


class BaseClass(object):
    def __init__(self):
        self.producer_data = KafkaProducer(
            bootstrap_servers=['h-kafka.inner.qury.me:9092'],
            api_version=(0, 10),
            value_serializer=lambda v: json.dumps(v).encode('utf-8'),
            key_serializer=lambda v: v.encode('utf-8'),
            security_protocol='SASL_PLAINTEXT', sasl_mechanism='SCRAM-SHA-256',
            sasl_plain_username="producer", sasl_plain_password='Iex@Ac@m&9U**Q%9')

        self.mongo = pymongo.MongoClient('mongodb://QurySpiderMongo:GI5oT8yckcFSGiwW@172.31.25.104:27099')

    def send_data2kafka(self, value, fa_name):
        """
        示例： kafka_producer.send('world', {'key1': 'value1'})
        :param fa_name:
        :param topic:     kafka 队列名称
        :param value:     python 字典类型
        :return:
        """
        topic = "mongo_receiver_topic"
        # print("fa_name", fa_name)
        try:
            if len(value.get('title', '')) > 0:
                # a = self.producer_data.send(topic, key=fa_name, value=value)
                # a.get(timeout=30)
                # self.mongo.h_spider.spider_data.update_one({'id': value.get('md5')}, {'$set': {'push_state': '1'}}, upsert=True)
                # print(a, 'mongo已修改')
                return True
            else:
                print('{} : {}, 没有title'.format(fa_name, value.get('id')))
                self.write_error_text(fa_name=fa_name, e='11111', text='没有title {}'.format(value.get('id', '')))
                return False
        except Exception as e:
            if isinstance(e, BufferError):
                print(e)
            else:
                print(traceback.print_exc())
            return False

    def close(self):
        self.producer_data.close()

    def mongo_data(self, version):
        try:
            print({'version': '%s' % version})
            m_data = self.mongo.h_spider.spider_data.find({'version': '%s' % version})
            return m_data
        except Exception as e:
            self.write_error_text(version, e, traceback.print_exc())

    def fa_data(self, data):
        try:
            fa_name = data.get('fa_name')
            item_mongo = dict()
            pass
            self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            traceback.print_exc()
            self.write_error_text(e, traceback.print_exc())
            print(e)

    def write_error_text(self, fa_name, e, text=''):
        with open('/home/ec2-user/jinbiao/fa_analysis/log/{}.txt'.format(fa_name), 'a+', encoding='utf-8') as ef:
            ef.write('{} <==> {}'.format(e, text) + '\n')

    def check_mongo_data(self, version):
        """ 查询mongo总数和推送成功的数（推送成功 push_state 字段从 '0' 改为 '1'） """
        num = 0
        m_data = self.mongo.h_spider.spider_data.find({'version': '%s' % version})
        print('mongo总数：', m_data.count())
        for i in m_data:
            if i.get('push_state') == '1':
                num += 1
        print('推送成功分总数：：', num)

    def run(self, version):
        data = self.mongo_data(version)
        for i in data:
            if i.get('push_state') == '1':
                print('数据以推送')
                continue
            self.fa_data(i)


class DemoCls(BaseClass):

    def send_data2kafka(self, value, fa_name):
        """
        示例： kafka_producer.send('world', {'key1': 'value1'})
        :param fa_name:
        :param topic:     kafka 队列名称
        :param value:     python 字典类型
        :return:
        """
        topic = "h_item"
        print("fa_name", fa_name)
        try:
            a = self.producer_data.send(topic, key=fa_name, value=value)
            a.get(timeout=30)
            # self.mongo.h_spider.spider_data.update_one({'id': value.get('md5')}, {'$set': {'push_state': '1'}}, upsert=True)
            print(a, 'mongo已修改')
        except Exception as e:
            if isinstance(e, BufferError):
                print(e)
            else:
                traceback.print_exc()

    def mongo_data(self, version):
        try:
            print({'version': '%s' % version})
            m_data = self.mongo.h_spider.spider_data.find({'version': '%s' % version}).limit(10)
            return m_data
        except Exception as e:
            self.write_error_text(version, e, traceback.print_exc())

    def write_error_text(self, fa_name, e, text=''):
        print(fa_name, e, text)


if __name__ == '__main__':
    version = '20210727-guazi-car'
    bc = BaseClass()
    bc.check_mongo_data(version)
