# coding=utf-8
import re

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import BaseClass, DemoCls


class Iqiyi_push(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')

        try:
            item_mongo = dict()
            item_mongo['title'] = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['tags'] = data.get('detail-data', {}).get('api_json', {}).get('new_tag', '')
            item_mongo['date_str'] = data.get('detail-data', {}).get('api_json', {}).get('date_str', '')
            item_mongo['img'] = data.get('detail-data', {}).get('api_json', {}).get('img', '')
            item_mongo['guide_price'] = data.get('detail-data', {}).get('api_json', {}).get('guide_price', '')
            item_mongo['frist_price'] = data.get('detail-data', {}).get('api_json', {}).get('frist_price', '')
            item_mongo['old_car_price'] = data.get('detail-data', {}).get('api_json', {}).get('old_car_price', '')
            item_mongo['degree'] = data.get('detail-data', {}).get('api_json', {}).get('degree', '')
            item_mongo['car_series'] = data.get('detail-data', {}).get('api_json', {}).get('car_series', '')
            item_mongo['level'] = data.get('detail-data', {}).get('api_json', {}).get('level', '')
            item_mongo['engine'] = data.get('detail-data', {}).get('api_json', {}).get('engine', '')
            item_mongo['car_color'] = data.get('detail-data', {}).get('api_json', {}).get('car_color', '')
            item_mongo['gearbox'] = data.get('detail-data', {}).get('api_json', {}).get('gearbox', '')
            item_mongo['cat_structure'] = data.get('detail-data', {}).get('api_json', {}).get('cat_structure', '')
            item_mongo['car_model'] = data.get('detail-data', {}).get('api_json', {}).get('car_model', '')
            item_mongo['cat_size'] = data.get('detail-data', {}).get('api_json', {}).get('cat_size', '')
            item_mongo['engine_type'] = data.get('detail-data', {}).get('api_json', {}).get('engine_type', '')
            item_mongo['drive_way'] = data.get('detail-data', {}).get('api_json', {}).get('drive_way', '')
            item_mongo['new_tag'] = data.get('detail-data', {}).get('api_json', {}).get('new_tag', '')
            item_mongo['brand'] = data.get('detail-data', {}).get('api_json', {}).get('brand', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['img'] = data.get('s3_img', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['city'] = '北京'
            item_mongo['source'] = 'path'
            item_mongo['country'] = ''
            item_mongo['hot'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['sale_count'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['province'] = ''
            item_mongo['area'] = ''
            item_mongo['deposit'] = ''
            item_mongo['score'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if i.get('channel') == '优惠购车':
                    if self.tengxun_data(i):
                        num += 1
                    else:
                        print('推送失败')
        print(num)


if __name__ == '__main__':
    version = '20210713-yiche-newcar'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
