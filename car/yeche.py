# coding=utf-8
import re

import sys
import time
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import BaseClass, DemoCls


class Iqiyi_push(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')

        try:
            item_mongo = dict()
            title_1 = data.get('list_data', {}).get('data_json', {}).get('title', '')
            title_2 = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['title'] = title_1 if title_1 else title_2

            tags_1 = data.get('list_data', {}).get('data_json', {}).get('tags', '')
            tags_2 = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            item_mongo['tags'] = tags_1 if tags_1 else tags_2

            date_str_1 = data.get('list_data', {}).get('data_json', {}).get('date', '')
            date_str_2 = data.get('detail-data', {}).get('api_json', {}).get('date', '')
            datestr_2 = re.findall('\d{4}-\d{1,2}-\d{1,2}', date_str_2)
            datestr_2 = datestr_2[0] if datestr_2 and len(datestr_2) > 0 else ''
            item_mongo['date_str'] = date_str_1 if date_str_1 else datestr_2

            item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('img', '')
            item_mongo['v_count'] = data.get('list_data', {}).get('data_json', {}).get('view', '')
            item_mongo['title_keyword'] = data.get('list_data', {}).get('data_json', {}).get('title_keyword', '')
            item_mongo['author'] = data.get('detail-data', {}).get('api_json', {}).get('author', '')
            item_mongo['c_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')
            item_mongo['detail'] = data.get('detail-data', {}).get('api_json', {}).get('subtitle', '')
            item_mongo['car_type'] = data.get('detail-data', {}).get('api_json', {}).get('car_type', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['url'] = data.get('url', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', 'path')
            item_mongo['adverb_tag'] = data.get('adverb_tag', '')

            item_mongo['intention'] = data.get('intention', '')
            item_mongo['type'] = data.get('type', '')
            item_mongo['agg_tags'] = data.get('agg_tags', '')
            item_mongo['province'] = data.get('province', '')
            item_mongo['city'] = data.get('city', '')
            item_mongo['area'] = data.get('area', '')
            item_mongo['g_price'] = data.get('g_price', '')

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def stamp_to_date(self, stamp):
        timeArray = time.localtime(stamp)
        otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
        return otherStyleTime

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                # if i.get('push_state') == '1' or i.get('channel') == '经销商' or i.get('channel') == '优惠购车':
                if i.get('channel') == '经销商' or i.get('channel') == '优惠购车':
                    # print('数据以推送')
                    continue
                if self.tengxun_data(i):
                    num += 1
                else:
                    print('推送失败')
        print(num)


if __name__ == '__main__':
    version = '20210713-yiche-newcar'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
