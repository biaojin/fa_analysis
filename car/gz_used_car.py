# coding=utf-8
import sys

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')
from base_class import BaseClass
import traceback


class Iqiyi_push(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()
            data_1 = data.get('list_data', {}).get('data_json', {})
            data_2 = data.get('detail-data', {}).get('api_json', {})

            item_mongo['title'] = data_1.get('title', '')
            item_mongo['car_series'] = data_1.get('title', '')
            img01 = data_1.get('img', '')
            img02 = data_1.get('thumb_img', '')
            item_mongo['img'] = img01 if img01 and len(img01) > 0 else img02
            item_mongo['price'] = data_1.get('price', '')
            frist_price = data_1.get('s_payments', '')
            if frist_price and '万' in frist_price:
                item_mongo['frist_price'] = frist_price
            elif frist_price and '万' not in frist_price:
                item_mongo['frist_price'] = str(frist_price) + '万'
            else:
                item_mongo['frist_price'] = ''

            tags_0 = data_2.get('tags', '')
            tags_1 = ','.join(data_2.get('tag_s', []))
            item_mongo['tags'] = str(tags_0) + str(tags_1)
            item_mongo['province'] = data_2.get('sendProvince', '')
            item_mongo['city'] = data_2.get('sendCity', '')
            item_mongo['area'] = data_2.get('sendCity', '')
            item_mongo['mileage'] = data_2.get('bxlc', '')
            item_mongo['smission_s'] = data_2.get('pzqy', '')
            item_mongo['displacement'] = data_2.get('qcpl', '')
            item_mongo['gearbox'] = data_2.get('bsx', '')
            item_mongo['car_color'] = data_2.get('csys', '')
            item_mongo['maintenan_rec'] = data_2.get('maintenance', '')
            item_mongo['license'] = data_2.get('cphs', '')
            item_mongo['transfers_n'] = data_2.get('ghcs', '')
            item_mongo['license_area'] = data_2.get('cphs', '')
            item_mongo['license_time'] = data_2.get('scsp', '')
            item_mongo['car_desc'] = data_2.get('car_desc', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['title_keyword'] = ''
            item_mongo['source'] = 'path'
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['car_model'] = ''
            item_mongo['m_rent'] = ''
            item_mongo['car_age'] = ''
            item_mongo['drive_way'] = ''
            item_mongo['engine'] = ''
            item_mongo['brand'] = ''
            item_mongo['new_tag'] = '0'
            item_mongo['config'] = ''
            item_mongo['hot'] = ''
            item_mongo['maintenan_type'] = ''
            item_mongo['interior_color'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if self.fa_data(i):
                    num += 1
                else:
                    print('推送失败')
            # if i.get('push_state') == '1':
            # else:
            #     print('数据以推送')
            #     continue
        print(num)


if __name__ == '__main__':
    version = '20210727-guazi-car'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
