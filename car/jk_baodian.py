# coding=utf-8
import time
import sys
sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')
from base_class import BaseClass


class Iqiyi_push(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()
            title_0 = data.get('list_data', {}).get('data_json', {}).get('title', '')
            title_1 = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['title'] = title_0 if title_0 else title_1

            img_0 = data.get('list_data', {}).get('data_json', {}).get('img', '')
            img_1 = data.get('detail-data', {}).get('api_json', {}).get('img', '')
            item_mongo['img'] = img_0 if img_0 else img_1

            score_0 = data.get('list_data', {}).get('data_json', {}).get('score', '')
            score_1 = data.get('detail-data', {}).get('api_json', {}).get('score', '')
            item_mongo['score'] = score_0 if score_0 else score_1

            student_0 = data.get('list_data', {}).get('data_json', {}).get('student', '')
            student_1 = data.get('detail-data', {}).get('api_json', {}).get('student', '')
            item_mongo['stu_n'] = student_0 if student_0 else student_1.replace('学员：', '').replace('名', '')

            item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('price', '')

            item_mongo['tags'] = data.get('detail-data', {}).get('api_json', {}).get('tag', '')
            item_mongo['date_str'] = data.get('detail-data', {}).get('api_json', {}).get('date', '')
            item_mongo['c_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '').replace('条评价',
                                                                                                               '')

            item_mongo['desc'] = data.get('detail-data', {}).get('api_json', {}).get('subtitle', '').replace('\n', '')

            operate = data.get('detail-data', {}).get('api_json', {}).get('operate', '')
            item_mongo['certify_tag'] = '1' if operate else '0'
            commer_tag = data.get('detail-data', {}).get('api_json', {}).get('lacc', '')
            item_mongo['commer_tag'] = '1' if commer_tag else '0'
            pick_up_tag = data.get('detail-data', {}).get('api_json', {}).get('tag', '')
            item_mongo['pick_up_tag'] = '1' if '有接送' in pick_up_tag else '0'
            item_mongo['g_reputate_tag'] = '1' if '口碑好' in pick_up_tag else '0'

            p_type = data.get('detail-data', {}).get('api_json', {}).get('type', '')
            p_car = data.get('detail-data', {}).get('api_json', {}).get('car', '')
            item_mongo['coach_s'] = str(p_type) + ' ' + str(p_car)

            item_mongo['coach_y'] = data.get('detail-data', {}).get('api_json', {}).get('age', '')
            item_mongo['driving_s'] = data.get('detail-data', {}).get('api_json', {}).get('school', '')

            item_mongo['url'] = data.get('url', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['vers'] = data.get('version', '')

            item_mongo['title_keyword'] = ''
            item_mongo['source'] = 'path'
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['province'] = ''
            item_mongo['area'] = ''
            item_mongo['city'] = '北京市'
            item_mongo['c_tag'] = '1'

            item_mongo['f_count'] = ''
            item_mongo['phone'] = ''
            item_mongo['train_area'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def stamp_to_date(self, stamp):
        timeArray = time.localtime(stamp)
        otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
        return otherStyleTime

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            # if i.get('push_state') == '1' or i.get('channel') == '找驾校' or i.get('channel') == '找教练':
            #     print('数据以推送')
            #     continue
            num += 1
            self.tengxun_data(i)
        print(num)


if __name__ == '__main__':
    version = '20210726-jiakao-android'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
