# coding=utf-8
import re

import pymongo
import time
import traceback
from kafka import KafkaProducer
import json
import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


# class TengXun(DemoCls):
class TengXun(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()
            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['tags'] = data.get('list_data', {}).get('data_json', {}).get('regiontype', '')
            item_mongo['area'] = data.get('list_data', {}).get('data_json', {}).get('vendorsaleaddr', '')
            item_mongo['major'] = data.get('list_data', {}).get('data_json', {}).get('mainbrand', '')
            item_mongo['desc'] = data.get('list_data', {}).get('data_json', {}).get('newstitle', '')
            item_mongo['desc'] = data.get('list_data', {}).get('data_json', {}).get('dealerphone', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['title_keyword'] = ''
            item_mongo['img'] = ''
            item_mongo['province'] = ''
            item_mongo['city'] = ''
            item_mongo['date_str'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['source'] = 'path'

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if i.get('channel') == '经销商':
                    num += 1
                    self.fa_data(i)
                # elif i.get('push_state') == '1':
                #     print('数据以推送')
                else:
                    print('数据以推送')
                    continue

            else:
                print('图片没有刷新', i.get('id'))
        print(num)

if __name__ == '__main__':
    version = '20210713-yiche-newcar'
    ddky = TengXun()
    ddky.run(version)
    ddky.check_mongo_data(version)
    ddky.close()

    # print(ddky.mongo.h_spider.spider_data.find_one({'version': '%s' % version}))

