# coding=utf-8
import sys
import traceback

import requests
from lxml import etree

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


class Iqiyi_push(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()
            item_mongo['title_keyword'] = data.get('title_keyword', '')
            item_mongo['intention'] = data.get('intention', '')
            item_mongo['type'] = data.get('type', '')
            item_mongo['agg_tags'] = data.get('agg_tags', '')
            item_mongo['tags'] = data.get('tags', '')
            item_mongo['province'] = data.get('province', '')
            item_mongo['city'] = data.get('city', '')
            item_mongo['area'] = data.get('area', '')
            item_mongo['v_tag'] = data.get('v_tag', '')
            item_mongo['p_tag'] = data.get('p_tag', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', 'path')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['md5'] = data.get('id', '')


            title = data.get('list_json', {}).get('data_json', {}).get('title', '')
            title_s = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['title'] = title if title and len(title) > 0 else title_s

            date_str = data.get('list_json', {}).get('data_json', {}).get('date_str', '')
            date_str_s = data.get('detail-data', {}).get('api_json', {}).get('date_str', '')
            item_mongo['date_str'] = date_str if date_str and len(date_str) > 0 else date_str_s

            singer = data.get('list_json', {}).get('data_json', {}).get('singer', '')
            singer_s = data.get('detail-data', {}).get('api_json', {}).get('singer', '')
            item_mongo['singer'] = singer if singer and len(singer) > 0 else singer_s

            duration = data.get('list_data', {}).get('data_json', {}).get('duration', '')
            duration_s = data.get('detail-data', {}).get('api_json', {}).get('duration', '')
            item_mongo['duration'] = duration if duration and len(duration) > 0 else duration_s

            album = data.get('list_json', {}).get('data_json', {}).get('album', '')
            album_s = data.get('detail-data', {}).get('api_json', {}).get('album', '')
            item_mongo['album'] = album if album and len(album) > 0 else album_s

            lanu = data.get('list_json', {}).get('data_json', {}).get('lanu', '')
            lanu_s = data.get('detail-data', {}).get('api_json', {}).get('album', '')
            item_mongo['lanu'] = lanu if lanu and len(lanu) > 0 else lanu_s

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['img'] = data.get('detail-data', {}).get('api_json', {}).get('img', '')
            item_mongo['sect'] = data.get('detail-data', {}).get('api_json', {}).get('sect', '')
            item_mongo['lyrics'] = data.get('detail-data', {}).get('api_json', {}).get('lyric', '')
            item_mongo['c_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')
            item_mongo['lyricist'] = data.get('detail-data', {}).get('api_json', {}).get('lyricist', '')
            item_mongo['composer'] = data.get('detail-data', {}).get('api_json', {}).get('composer', '')
            item_mongo['r_company'] = data.get('detail-data', {}).get('api_json', {}).get('lanr_companyu', '')

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                # if i.get('push_state') == '1':
                #     print('数据以推送')
                #     continue
                if self.tengxun_data(i):
                    num += 1
            else:
                print('图片没有刷新')
        print(num)


if __name__ == '__main__':
    version = '20210713-qq-music'
    iqiyi = Iqiyi_push()
    iqiyi.run(version)
    iqiyi.close()
