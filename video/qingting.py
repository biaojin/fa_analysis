#
#
#
# # coding=utf-8
# import re
#
# import pymongo
# import requests
# import time
# from lxml import etree
#
#
# import traceback
# from kafka import KafkaProducer
# import json
#
#
# def get_anchor_desc(user_id):
#     try:
#         if user_id and len(user_id) > 0:
#             url = 'https://www.qingting.fm/podcasters/{}/'.format(user_id)
#             headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'}
#             con = requests.get(url=url, headers=headers, timeout=(10, 15)).text
#             data = etree.HTML(con)
#             img = data.xpath('//div[@class="podcaster-top-info"]/img/@src')
#             img = 'https:' + img[0] if img and len(img) > 0 else ''
#             desc = data.xpath('//div[@class="podcaster-top-info"]//text()')
#             desc = ','.join(desc) if desc and len(desc) > 0 else ''
#             return img, desc.replace('\n', '').replace('\r', '')
#         else:
#             return '', ''
#     except Exception as e:
#         print('请求主播简介和图片报错', traceback.print_exc())
#         return '', ''
#
#
# class Iqiyi_push(object):
#     def __init__(self):
#         self.producer_data = KafkaProducer(
#             bootstrap_servers=['h-kafka.inner.qury.me:9092'],
#             api_version=(0, 10),
#             value_serializer=lambda v: json.dumps(v).encode('utf-8'),
#             key_serializer=lambda v: v.encode('utf-8'),
#             security_protocol='SASL_PLAINTEXT', sasl_mechanism='SCRAM-SHA-256',
#             sasl_plain_username="producer", sasl_plain_password='Iex@Ac@m&9U**Q%9')
#
#         self.mongo = pymongo.MongoClient('mongodb://QurySpiderMongo:GI5oT8yckcFSGiwW@172.31.25.104:27099')
#
#     def parse(self, data):
#         print(111111111111,data)
#         self.send_data2kafka(data, '蜻蜓FM')
#
#     def send_data2kafka(self, value, fa_name):
#         """
#         示例： kafka_producer.send('world', {'key1': 'value1'})
#         :param topic:     kafka 队列名称
#         :param value:     python 字典类型
#         :return:
#         """
#         topic = "h_item"
#         print("fa_name", fa_name)
#         try:
#             a = self.producer_data.send(topic, key=fa_name, value=value)
#             a.get(timeout=30)
#             self.mongo.h_spider.spider_data.update_one({'id': value.get('md5')}, {'$set': {'img': value.get('img')}},upsert=True)
#             print(a, 'mongo已修改')
#         except Exception as e:
#             if isinstance(e, BufferError):
#                 print(e)
#             else:
#                 traceback.print_exc()
#
#     def close(self):
#         self.producer_data.close()
#
#     def run(self, data):
#         self.parse(data)
#
#
#
# def get_mongo_data(version):
#     print({'version': '%s' % version})
#     m_data = iqiyi.mongo.h_spider.spider_data.find({'version': '%s' % version})
#     return m_data
#
#
# def stamp_to_date(stamp):
#     timeArray = time.localtime(stamp)
#     otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(stamp))
#     return otherStyleTime
#
#
# def analysis(version):
#     data_list = list()
#     m_data = get_mongo_data(version)
#     for data in m_data:
#         try:
#             if data.get('push_state') == '1':
#                 print('数据以推送')
#                 continue
#             item_mongo = dict()
#             item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
#             item_mongo['title_keyword'] = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('seo', {}).get('keywords', '')
#             item_mongo['fa_name'] = data.get('fa_name', '')
#             item_mongo['s_path'] = data.get('s_path', '')
#             item_mongo['source'] = data.get('', 'path')
#             item_mongo['md5'] = data.get('id', '')
#             item_mongo['url'] = data.get('url', '')
#             item_mongo['intention'] = data.get('', '')
#             item_mongo['type'] = data.get('', '')
#             item_mongo['agg_tags'] = data.get('', '')
#             date_str = data.get('list_data', {}).get('data_json', {}).get('update_time', '')
#             item_mongo['date_str'] = stamp_to_date(int(time.mktime(time.strptime(date_str, "%Y-%m-%d %H:%M:%S")))) if date_str and len( date_str) > 0 else ''
#             item_mongo['province'] = data.get('', '')
#             item_mongo['city'] = data.get('', '')
#             item_mongo['area'] = data.get('', '')
#             item_mongo['vers'] = data.get('version', '')
#             item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('cover', '')
#             author = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album', {}).get('podcasters', [{}])
#             if author and len(author) > 0:
#                 item_mongo['author'] = author[0].get('name', '')
#             else:
#                 item_mongo['author'] = ''
#             duration = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('plist', [{}])
#             if duration and len(duration) > 0:
#                 item_mongo['duration'] = duration[0].get('duration', '')
#             else:
#                 item_mongo['duration'] = ''
#
#             item_mongo['program_n'] = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album', {}).get('program_count', '')
#             item_mongo['c_count'] = data.get('', '')
#             p_count = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album', {}).get('playcount', '')
#             item_mongo['p_count'] = int(float(p_count.replace('万', ''))) * 10000 if '万' in p_count else p_count if '万' in p_count else p_count
#             item_mongo['album_desc'] = data.get('list_data', {}).get('data_json', {}).get('description', '')
#             item_mongo['score'] = data.get('list_data', {}).get('data_json', {}).get('score', '')
#             n_anchor = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album', {}).get( 'podcasters', [{}])
#             if n_anchor and len(n_anchor) > 0:
#                 item_mongo['n_anchor'] = n_anchor[0].get('name', '')
#             else:
#                 item_mongo['n_anchor'] = ''
#
#             item_mongo['d_count'] = data.get('', '')
#             user_id = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album', {}).get('podcasters', [{}])
#             if user_id and len(user_id) > 0:
#                 user_id = user_id[0].get('user_id', '')
#             else:
#                 user_id = ''
#             item_mongo['n_anchor_img'], item_mongo['anchor_desc'] = get_anchor_desc(user_id)
#             item_mongo['album'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
#             update_time = data.get('list_data', {}).get('data_json', {}).get('update_time', '')
#             item_mongo['update_time'] = stamp_to_date(int(time.mktime(time.strptime(update_time, "%Y-%m-%d %H:%M:%S")))) if update_time and len(update_time) > 0 else ''
#             tag = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album',{}).get('attrs', [])
#             name_list = list()
#             if tag and isinstance(tag, list) and len(tag) > 0:
#                 for t in tag:
#                     name = t.get('name', '')
#                     name_list.append(name)
#             else:
#                 name_list = ''
#             item_mongo['tags'] = ' '.join(name_list)
#
#             page = data.get('page', '')
#             rank = data.get('rank', '')
#             if page and rank:
#                 item_mongo['rank'] = int(page) * int(rank)
#             elif rank and not page:
#                 item_mongo['rank'] =int(rank)
#             else:
#                 item_mongo['rank'] = ''
#             iqiyi.run(item_mongo)
#         except Exception as e:
#             print(traceback.print_exc())
#     return data_list
#
#
# if __name__ == '__main__':
#     iqiyi = Iqiyi_push()
#     version = '20210708-qingting-FM'
#     analysis(version)
#     iqiyi.close()
#


import sys
import traceback

import requests
from lxml import etree

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


def get_anchor_desc(user_id):
    try:
        if user_id and len(user_id) > 0:
            url = 'https://www.qingting.fm/podcasters/{}/'.format(user_id)
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'}
            con = requests.get(url=url, headers=headers, timeout=(10, 15)).text
            data = etree.HTML(con)
            img = data.xpath('//div[@class="podcaster-top-info"]/img/@src')
            img = 'https:' + img[0] if img and len(img) > 0 else ''
            desc = data.xpath('//div[@class="podcaster-top-info"]//text()')
            desc = ','.join(desc) if desc and len(desc) > 0 else ''
            return img, desc.replace('\n', '').replace('\r', '')
        else:
            return '', ''
    except Exception as e:
        print('请求主播简介和图片报错', traceback.print_exc())
        return '', ''


# class TengXun(DemoCls):
class TengXun(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['title_keyword'] = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get(
                'channelPage', {}).get('seo', {}).get('keywords', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('', 'path')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['intention'] = data.get('', '')
            item_mongo['type'] = data.get('', '')
            item_mongo['agg_tags'] = data.get('', '')
            date_str = data.get('list_data', {}).get('data_json', {}).get('update_time', '')
            item_mongo['date_str'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                (int(time.mktime(time.strptime(date_str, "%Y-%m-%d %H:%M:%S")))))) if date_str and len(
                date_str) > 0 else ''
            item_mongo['province'] = data.get('', '')
            item_mongo['city'] = data.get('', '')
            item_mongo['area'] = data.get('', '')
            item_mongo['vers'] = data.get('version', '')
            # item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('cover', '')
            item_mongo['img'] = data.get('s3_img', '')
            author = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album',
                                                                                                                {}).get(
                'podcasters', [{}])
            if author and len(author) > 0:
                item_mongo['author'] = author[0].get('name', '')
            else:
                item_mongo['author'] = ''
            duration = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get(
                'plist', [{}])
            if duration and len(duration) > 0:
                item_mongo['duration'] = duration[0].get('duration', '')
            else:
                item_mongo['duration'] = ''

            item_mongo['program_n'] = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage',
                                                                                                          {}).get(
                'album', {}).get('program_count', '')
            item_mongo['c_count'] = data.get('', '')
            p_count = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get(
                'album', {}).get('playcount', '')
            item_mongo['p_count'] = int(
                float(p_count.replace('万', ''))) * 10000 if '万' in p_count else p_count if '万' in p_count else p_count
            item_mongo['album_desc'] = data.get('list_data', {}).get('data_json', {}).get('description', '')
            item_mongo['score'] = data.get('list_data', {}).get('data_json', {}).get('score', '')
            n_anchor = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get(
                'album', {}).get('podcasters', [{}])
            if n_anchor and len(n_anchor) > 0:
                item_mongo['n_anchor'] = n_anchor[0].get('name', '')
            else:
                item_mongo['n_anchor'] = ''

            item_mongo['d_count'] = data.get('', '')
            user_id = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get(
                'album', {}).get('podcasters', [{}])
            if user_id and len(user_id) > 0:
                user_id = user_id[0].get('user_id', '')
            else:
                user_id = ''
            item_mongo['n_anchor_img'], item_mongo['anchor_desc'] = '', ''  # get_anchor_desc(user_id)
            item_mongo['album'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            update_time = data.get('list_data', {}).get('data_json', {}).get('update_time', '')
            item_mongo['update_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                (int(time.mktime(time.strptime(update_time, "%Y-%m-%d %H:%M:%S")))))) if update_time and len(
                update_time) > 0 else ''
            tag = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('channelPage', {}).get('album',
                                                                                                             {}).get(
                'attrs', [])
            name_list = list()
            if tag and isinstance(tag, list) and len(tag) > 0:
                for t in tag:
                    name = t.get('name', '')
                    name_list.append(name)
            else:
                name_list = ''
            item_mongo['tags'] = ' '.join(name_list)

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''
            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        try:
            for i in data:
                if len(i.get('s3_img', '')) > 0:
                    # if i.get('push_state') == '1':
                    #     print('数据以推送')
                    if len(i.get('list_data', {}).get('data_json', {}).get('title')) <= 0:
                        print(i)
                        continue
                    if self.fa_data(i):
                        num += 1
                else:
                    print('图片没有刷新', i.get('id'))
        except Exception as e:
            print(e)
        print(num)


if __name__ == '__main__':
    version = '20210708-qingting-FM'
    ddky = TengXun()
    ddky.run(version)
    # ddky.check_mongo_data(version)
    ddky.close()
