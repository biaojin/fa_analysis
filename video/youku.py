# coding=utf-8
import sys
import traceback

import requests

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


def get_count(v_id):
    try:
        if v_id and len(v_id) > 0:
            url = 'https://m.youku.com/video/{}.html'.format(v_id)
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'}
            tunnel = "tps215.kdlapi.com:15818"
            # 用户名密码方式
            username = "t12504037386689"
            password = "jgnijmh5"
            proxies = {
                "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
                "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}}
            con = requests.get(url=url, headers=headers)
            if con.status_code == 200:
                count = re.findall('"totalComment":(.*?),"', con.text, re.S)
            else:
                con = requests.get(url=url, headers=headers, proxies=proxies)
                if con.status_code == 200:
                    count = re.findall('"totalComment":(.*?),"', con.text, re.S)
                else:

                    count = ''
            count = count[0] if count and len(count) > 0 else ''
            return count
    except Exception as e:
        print(e)


class YouKu(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()
            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['title_keyword'] = data.get('', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', 'path')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['intention'] = data.get('intention', '')
            item_mongo['type'] = data.get('type', '')
            item_mongo['agg_tags'] = data.get('agg_tags', '')
            item_mongo['province'] = data.get('province', '')
            item_mongo['city'] = data.get('city', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['t_type'] = data.get('t_type', '')
            item_mongo['lanu'] = data.get('detail-data', {}).get('api_json', {}).get('lanu', '')
            # item_mongo['c_count'] = get_count(data.get('list_data', {}).get('data_json', {}).get('videoId', ''))
            item_mongo['c_count'] = ''

            # img = data.get('list_data', {}).get('data_json', {}).get('img', '')
            # item_mongo['img'] = 'https:' + img if img and img.startswith('//') else ''
            item_mongo['img'] = data.get('s3_img', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''
            item_mongo['director'] = data.get('detail-data', {}).get('api_json', {}).get('director', '')
            major = data.get('detail-data', {}).get('api_json', {}).get('major', '')
            item_mongo['major'] = major.replace('简介元数据,', '') if major and len(str(major)) > 0 else ''
            item_mongo['tags'] = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            area = data.get('detail-data', {}).get('api_json', {}).get('area', '')
            item_mongo['area'] = ' '.join(area) if area and isinstance(area, list) and len(area) > 0 else ''
            item_mongo['intr'] = data.get('detail-data', {}).get('api_json', {}).get('intr', '')
            item_mongo['score'] = data.get('detail-data', {}).get('api_json', {}).get('score', '')
            date_str = data.get('detail-data', {}).get('api_json', {}).get('date_str', '')
            date_str = str(date_str).replace('0000-00-00', '').split(' ')
            if len(date_str) > 0:
                date = date_str[0]
                if len(date) > 4:
                    item_mongo['date_str'] = stamp_to_date(
                        int(time.mktime(
                            time.strptime(str(date.replace('-00', '-01')).replace('"', '').split(' ')[0], "%Y-%m-%d"))))
                if len(date) == 4:
                    item_mongo['date_str'] = stamp_to_date(
                        int(time.mktime(time.strptime(str(date).replace('"', '').split(' ')[0], "%Y"))))

            else:
                item_mongo['date_str'] = ''

            s_tag = data.get('detail-data', {}).get('api_json', {}).get('vsp_tag', '')
            item_mongo['v_tag'] = s_tag if 'VIP' in s_tag else ''
            item_mongo['s_tag'] = s_tag if '独播' in s_tag else ''
            item_mongo['p_tag'] = s_tag if '自制' in s_tag else ''
            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if self.fa_data(i):
                    num += 1
            else:
                print('图片没有刷新', i.get('id'))
        print(num)


if __name__ == '__main__':
    iqiyi = YouKu()
    # version = '20210722-youku-movie'  # 第一版本号
    version = '20210813-youku-movie'  # 补充版本号
    iqiyi.run(version)
    iqiyi.close()
