# coding=utf-8
import sys
import traceback

import requests

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


class Iqiyi_push(BaseClass):
    def mongo_data(self, version):
        num = []
        print({'version': '%s' % version})
        m_data = self.mongo.h_spider.spider_data.find({'version': '%s' % version})
        for k in m_data:
            path = k.get('id', '')
            num.append(path)
        print(len(num))
        print(len(set(num)))

    def find_one(self, url_id):
        m_data = self.mongo.h_spider.spider_data.find_one(
            {'version': '20200805-ctrip-android-view', 'id': url_id})
        # m_data['list_data'] = ''
        m_data['detail-data'] = ''
        print(m_data)


if __name__ == '__main__':
    urls = ["xiecheng_0d062b453ce70e4575cfb8ce5461d9bd",
            "xiecheng_1bd5177dfeb5173d1e1b7f0dacbdb6cf",
            "xiecheng_aabab4b6c640fcc638115be56b054d10",
            "xiecheng_847876a80be17be70fd42ef8c1f40e00",
            "xiecheng_3cdc39a94dcffafb869d0dfa2ac878a6",
            "xiecheng_05be69c1310d22c6d2dd6bff1075a902",
            "xiecheng_8ad6628470ee30f578d637bedc0ea460",
            "xiecheng_1d6a05ccc05a35670922ea16e5dd1401",
            "xiecheng_296ec5b09e525dc5f742a054da066098",
            "xiecheng_4bd65aabc736f6d38b5fced97a1bb689",
            "xiecheng_65f06bd6424a9ccb54e90e97ce1266ef",
            "xiecheng_9812e7f4ce750a17c7d13e99e1ecc0ba",
            "xiecheng_352458ca048b3c1864141cf1ccde6503",
            "xiecheng_0e8cd56f684761765a6051b2fa301a31",
            "xiecheng_a13a0faccaac7218fa4f97debd267053",
            "xiecheng_55e96236e2d411365fc4c8b3ce59588a",
            "xiecheng_8ca21f2a281db2906bc33bc99ccb9313",
            "xiecheng_b931e7ce138633429b1c63bc24a77a4c",
            "xiecheng_8af9c8150548dd45a2f23e8a2d3cd8ac",
            "xiecheng_db689bae4467d546d5934c3dd4746a21",
            "xiecheng_a6ca9e514e0655db7d35e0fd8c3aab00",
            "xiecheng_1e44e89a3fa6479bab826ba7f9c325e6",
            "xiecheng_97a06fb3b4d87294438cb04a5b900375",
            "xiecheng_7f218bae89640df4709fad22ca2a3fb1",
            "xiecheng_ef3f611b24ebc89036d86c4d7d812869",
            "xiecheng_7e30a3d332d1375d23a1fea3d314e51e",
            "xiecheng_d2d28cca62b8587cf049ccf0f9075216",
            "xiecheng_59a2c9b4fb76e72e8abe2d42852fa13d"]
    version = '20210813-youku-movie'
    iqiyi = Iqiyi_push()
    # iqiyi.mongo_data(version)
    for i in urls:
        iqiyi.find_one(i)

    """
携程旅行 : 共541668
携程旅行*攻略/景点 : 414509
携程旅行*旅游*目的地推荐 : 127159

推送成功：
    携程旅行：474432
    携程旅行*旅游*目的地推荐 : 127125
    携程旅行*攻略/景点 : 347307

失败 ：67236
    """
