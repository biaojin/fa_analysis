# coding=utf-8
import pymongo
import time
import re
import traceback
from kafka import KafkaProducer
import json

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import BaseClass, DemoCls, stamp_to_date


# class JingDong(DemoCls):
class JingDong(BaseClass):

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 1:
                channel = i.get('channel')
                if channel == '听书':
                    print(2222, channel)
                    continue
                if self.fa_data(i):
                    num += 1
            else:
                print(i.get('channel'))
                print('没有刷新图片')
        print(num)

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['md5'] = data.get('id', '')
            title = data.get('list_data', {}).get('data_json', {}).get('songname', '')
            title1 = data.get('list_data', {}).get('data_json', {}).get('FileName', '')
            item_mongo['title'] = title if title and len(str(title)) > 0 else title1
            singer = data.get('list_data', {}).get('data_json', {}).get('singername', '')
            singer1 = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('author_name', '')
            item_mongo['singer'] = singer if singer and len(str(singer)) > 0 else singer1
            item_mongo['duration'] = data.get('list_data', {}).get('data_json', {}).get('duration', '')
            date_str = data.get('list_data', {}).get('data_json', {}).get('publish_date', '')
            item_mongo['date_str'] = stamp_to_date(
                int(time.mktime(time.strptime(str(date_str), "%Y-%m-%d")))) if date_str and len(date_str) > 4 else ''
            item_mongo['album'] = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('album_name', '')
            img = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('img', '')
            item_mongo['img'] = img if img and img.startswith('http') else ''
            item_mongo['title_keyword'] = data.get('keyword', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', 'path')
            item_mongo['url'] = data.get('url', '')
            item_mongo['intention'] = data.get('intention', '')
            item_mongo['type'] = data.get('type', '')
            item_mongo['agg_tags'] = data.get('agg_tags', '')
            item_mongo['tags'] = data.get('tags', '')
            item_mongo['province'] = data.get('province', '')
            item_mongo['city'] = data.get('city', '')
            item_mongo['area'] = data.get('area', '')
            item_mongo['vers'] = data.get('version', '')
            item_mongo['c_count'] = data.get('c_count', '')
            item_mongo['sect'] = data.get('sect', '')
            item_mongo['v_tag'] = data.get('v_tag', '')
            item_mongo['p_tag'] = data.get('p_tag', '')
            item_mongo['lanu'] = data.get('lanu', '')
            item_mongo['r_company'] = data.get('r_company', '')
            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            lyrics = data.get('detail-data', {}).get('api_json', {}).get('data', {}).get('lyrics', '')
            item_mongo['lyrics'] = ', '.join(
                re.sub('\[(.*?)\]', '', lyrics).replace('\ufeff', '').strip().split('\r\n')) if lyrics and len(
                str(lyrics)) > 0 else ''
            lyricist = re.findall('作词：(.*?),', item_mongo['lyrics'])
            item_mongo['lyricist'] = lyricist[0] if lyricist and len(lyricist) > 0 else ''
            composer = re.findall('作曲：(.*?),', item_mongo['lyrics'])
            item_mongo['composer'] = composer[0] if composer and len(composer) > 0 else ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(traceback.print_exc())
            self.write_error_text(fa_name, e, traceback.print_exc())


if __name__ == '__main__':
    version = '20210708-kugou-music'
    ddky = JingDong()
    ddky.run(version)
    ddky.close()
