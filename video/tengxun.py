# coding=utf-8

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import *


# class TengXun(DemoCls):
class TengXun(BaseClass):

    def fa_data(self, data):
        fa_name = data.get('fa_name', '')
        try:
            item_mongo = dict()

            item_mongo['md5'] = data.get('id', '')
            item_mongo['title'] = data.get('list_data', {}).get('data_json').get('title', '')
            item_mongo['channel'] = data.get('channel', '')
            item_mongo['title_keyword'] = data.get('title_keyword', '')
            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', 'path')
            item_mongo['url'] = data.get('url', '')
            item_mongo['intention'] = data.get('intention', '')
            item_mongo['type'] = data.get('type', '')
            item_mongo['t_type'] = data.get('t_type', '')
            item_mongo['agg_tags'] = data.get('agg_tags', '')
            # tags01 = data.get('detail-data', {}).get('json', {}).get('tags', '')
            tags01 = ''
            tags02 = data.get('detail-data', {}).get('api_json', {}).get('tags', '')
            item_mongo['tags'] = tags01 if tags01 and len(tags01) > 0 else tags02

            # u_date01 = data.get('detail-data', {}).get('json', {}).get('u_date', '')
            u_date01 = ''
            u_date02 = data.get('detail-data', {}).get('api_json', {}).get('u_date', '')
            u_date = u_date02 if u_date02 and len(u_date02) > 4 else u_date01
            item_mongo['u_date'] = stamp_to_date(int(time.mktime(
                time.strptime(u_date.replace('/', '-').replace('日', '-').replace('月', '-').replace('年', '-'),
                              "%Y-%m-%d")))) if u_date and len(u_date) > 4 else ''

            # update_date01 = data.get('detail-data', {}).get('json', {}).get('update_date', '')
            update_date01 = ''
            update_date02 = data.get('detail-data', {}).get('api_json', {}).get('update_date', '')
            update_date = update_date02 if update_date02 and len(update_date02) > 4 else update_date01
            item_mongo['update_date'] = stamp_to_date(int(time.mktime(
                time.strptime(update_date.replace('/', '-').replace('日', '-').replace('月', '-').replace('年', '-'),
                              "%Y-%m-%d")))) if update_date and len(
                update_date) > 4 else ''

            # date_str01 = data.get('detail-data', {}).get('json', {}).get('date_str', '')
            date_str01 = ''
            date_str02 = data.get('detail-data', {}).get('api_json', {}).get('date_str', '')
            date_str = date_str02 if date_str02 and len(date_str02) > 4 else date_str01
            item_mongo['date_str'] = stamp_to_date(int(time.mktime(
                time.strptime(date_str.replace('/', '-').replace('日', '-').replace('月', '-').replace('年', '-'),
                              "%Y-%m-%d")))) if date_str and len(date_str) > 4 else ''

            score = data.get('list_data', {}).get('data_json', {}).get('score', '')
            # score1 = data.get('detail-data', {}).get('json', {}).get('score', '')
            score1 = ''
            item_mongo['score'] = str(score) if score and len(str(score)) > 0 else str(score1)

            # vsp_tag01 = data.get('detail-data', {}).get('json', {}).get('imgtag_ver', '')
            vsp_tag01 = ''
            vsp_tag02 = data.get('detail-data', {}).get('api_json', {}).get('imgtag_ver', '')
            vsp_tag = vsp_tag01 if vsp_tag01 else vsp_tag02
            item_mongo['v_tag'] = vsp_tag if 'VIP' in vsp_tag else ''
            item_mongo['s_tag'] = vsp_tag if '超前' in vsp_tag else ''
            item_mongo['p_tag'] = vsp_tag if '自制' in vsp_tag else ''

            v_count = data.get('list_data', {}).get('data_json').get('v_count', '')
            # v_count1 = data.get('detail-data', {}).get('json', {}).get('c_count', '')
            v_count1 = ''
            v_count2 = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')
            if v_count and len(v_count) > 0:
                item_mongo['v_count'] = v_count
            elif v_count1 and len(v_count1) > 0:
                item_mongo['v_count'] = v_count1
            else:
                item_mongo['v_count'] = v_count2

            item_mongo['province'] = data.get('province', '')
            item_mongo['city'] = data.get('city', '')
            # area01 = data.get('detail-data', {}).get('json', {}).get('area', '')
            area01 = ''
            area02 = data.get('detail-data', {}).get('api_json', {}).get('area', '')
            item_mongo['area'] = area01 if area01 else area02
            item_mongo['vers'] = data.get('version', '')

            # alias_n01 = data.get('detail-data', {}).get('json', {}).get('alias_n', '')
            alias_n01 = ''
            alias_n02 = data.get('detail-data', {}).get('api_json', {}).get('alias_n', '')
            item_mongo['alias_n'] = alias_n01 if area01 else alias_n02

            # e_name01 = data.get('detail-data', {}).get('json', {}).get('e_name', '')
            e_name01 = ''
            e_name02 = data.get('detail-data', {}).get('api_json', {}).get('e_name', '')
            item_mongo['e_name'] = e_name01 if e_name01 else e_name02

            # director01 = data.get('detail-data', {}).get('json', {}).get('director', '')
            director01 = ''
            director02 = data.get('detail-data', {}).get('api_json', {}).get('director', '')
            item_mongo['director'] = director01 if director01 else director02

            # intr01 = data.get('detail-data', {}).get('json', {}).get('intr', '')
            intr01 = ''
            intr02 = data.get('detail-data', {}).get('api_json', {}).get('intr', '')
            item_mongo['intr'] = intr01 if intr01 else intr02

            # lanu01 = str(data.get('detail-data', {}).get('json', {}).get('lanu', '')).replace('None', '').replace('null', '')
            lanu01 = ''
            lanu02 = str(data.get('detail-data', {}).get('api_json', {}).get('lanu', '')).replace('None', '').replace(
                'null', '')
            item_mongo['lanu'] = lanu01 if lanu01 else lanu02

            # major01 = data.get('detail-data', {}).get('json', {}).get('leading_actor', '')
            major01 = ''
            major02 = data.get('detail-data', {}).get('api_json', {}).get('leading_actor', '')
            item_mongo['major'] = major01 if major01 else major02

            item_mongo['img'] = data.get('s3_img', '')

            item_mongo['host'] = data.get('host', '')

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            self.write_error_text(fa_name, e, traceback.print_exc())

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                # if i.get('push_state') == '1':
                #     print('数据以推送')
                if len(i.get('list_data', {}).get('data_json', {}).get('title')) <= 0:
                    print(i)
                    continue
                if self.fa_data(i):
                    num += 1
            else:
                print('图片没有刷新', i.get('id'))
        print(num)


if __name__ == '__main__':
    version = '20210721-tencent-video'
    ddky = TengXun()
    ddky.run(version)
    # ddky.check_mongo_data(version)
    ddky.close()

    # print(ddky.mongo.h_spider.spider_data.find_one({'version': '%s' % version}))
