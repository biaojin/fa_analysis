# coding=utf-8
import re

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import BaseClass, DemoCls


class DdSy(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')

        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('img', '')
            item_mongo['department'] = data.get('list_data', {}).get('data_json', {}).get('manufacturer', '')
            item_mongo['c_count'] = data.get('list_data', {}).get('data_json', {}).get('c_count', '')
            item_mongo['specifications'] = data.get('list_data', {}).get('data_json', {}).get('productSize', '')

            item_mongo['desc'] = data.get('detail-data', {}).get('api_json', {}).get('dosage', '')
            item_mongo['medicine'] = data.get('detail-data', {}).get('api_json', {}).get('title', '')
            item_mongo['sub_title'] = data.get('detail-data', {}).get('api_json', {}).get('subtitle', '')
            item_mongo['manual'] = data.get('detail-data', {}).get('api_json', {}).get('instructions', '')

            price = data.get('list_data', {}).get('data_json', {}).get('price', '')
            if price and len(str(price)) > 0:
                item_mongo['price'] = '¥ ' + str(price)
            else:
                item_mongo['price'] = price

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['vers'] = data.get('version', '')
            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            item_mongo['title_keyword'] = ''
            item_mongo['articles_from'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['province'] = ''
            item_mongo['city'] = ''
            item_mongo['area'] = ''
            item_mongo['author'] = ''
            item_mongo['hospital'] = ''
            item_mongo['v_count'] = ''
            item_mongo['s_count'] = ''
            item_mongo['disease_site'] = ''
            item_mongo['symptom'] = ''
            item_mongo['cause'] = ''
            item_mongo['t_type'] = ''
            item_mongo['alias'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if self.tengxun_data(i):
                    num += 1
                else:
                    print('失败')
        print(num)


if __name__ == '__main__':
    version = '20210823-jiankemall'
    iqiyi = DdSy()
    iqiyi.run(version)
    iqiyi.close()
