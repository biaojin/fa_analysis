# coding=utf-8
import re

import sys
import traceback

sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')

from base_class import BaseClass, DemoCls


class DdSy(BaseClass):

    def tengxun_data(self, data):
        fa_name = data.get('fa_name', '')

        try:
            item_mongo = dict()

            item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('img', '')
            item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('price', '')
            item_mongo['medicine'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
            item_mongo['sub_title'] = data.get('list_data', {}).get('data_json', {}).get('subtitle', '')
            item_mongo['specifications'] = data.get('list_data', {}).get('data_json', {}).get('norms', '')

            item_mongo['department'] = data.get('detail-data', {}).get('api_json', {}).get('firm', '')
            item_mongo['desc'] = data.get('detail-data', {}).get('api_json', {}).get('details', '')
            item_mongo['c_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')
            item_mongo['s_count'] = data.get('detail-data', {}).get('api_json', {}).get('s_count', '')

            item_mongo['fa_name'] = data.get('fa_name', '')
            item_mongo['md5'] = data.get('id', '')
            item_mongo['url'] = data.get('url', '')
            item_mongo['s_path'] = data.get('s_path', '')
            item_mongo['source'] = data.get('source', '')
            item_mongo['vers'] = data.get('version', '')

            item_mongo['province'] = ''
            item_mongo['city'] = ''
            item_mongo['area'] = ''
            item_mongo['title_keyword'] = ''
            item_mongo['intention'] = ''
            item_mongo['type'] = ''
            item_mongo['agg_tags'] = ''
            item_mongo['tags'] = ''
            item_mongo['date_str'] = ''
            item_mongo['author'] = ''
            item_mongo['hospital'] = ''
            item_mongo['articles_from'] = ''
            item_mongo['v_count'] = ''
            item_mongo['manual'] = ''
            item_mongo['disease_site'] = ''
            item_mongo['symptom'] = ''
            item_mongo['cause'] = ''
            item_mongo['t_type'] = ''
            item_mongo['alias'] = ''

            page = data.get('page', '')
            rank = data.get('rank', '')
            if page and rank:
                item_mongo['rank'] = int(page) * int(rank)
            elif rank and not page:
                item_mongo['rank'] = int(rank)
            else:
                item_mongo['rank'] = ''

            return self.send_data2kafka(item_mongo, fa_name)
        except Exception as e:
            print(e)

    def run(self, version):
        num = 0
        data = self.mongo_data(version)
        for i in data:
            if len(i.get('s3_img', '')) > 0:
                if self.tengxun_data(i):
                    num += 1
                else:
                    print('推送失败')
        print(num)


if __name__ == '__main__':
    version = '20210823-ddsy'
    iqiyi = DdSy()
    iqiyi.run(version)
    iqiyi.close()

# import sys
# import traceback
#
# sys.path.append('/home/ec2-user/jinbiao/fa_analysis/')
#
# from base_class import BaseClass, DemoCls
#
#
# # class DdKy(DemoCls):
# class DdKy(BaseClass):
#
#     def fa_data(self, data):
#         fa_name = data.get('fa_name', '')
#         try:
#             item_mongo = dict()
#
#             item_mongo['title'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
#             item_mongo['img'] = data.get('list_data', {}).get('data_json', {}).get('img', '')
#             item_mongo['price'] = data.get('list_data', {}).get('data_json', {}).get('price', '')
#             item_mongo['medicine'] = data.get('list_data', {}).get('data_json', {}).get('title', '')
#             item_mongo['sub_title'] = data.get('list_data', {}).get('data_json', {}).get('subtitle', '')
#             item_mongo['specifications'] = data.get('list_data', {}).get('data_json', {}).get('norms', '')
#
#             item_mongo['department'] = data.get('detail-data', {}).get('api_json', {}).get('firm', '')
#             item_mongo['desc'] = data.get('detail-data', {}).get('api_json', {}).get('details', '')
#             item_mongo['c_count'] = data.get('detail-data', {}).get('api_json', {}).get('c_count', '')
#             item_mongo['s_count'] = data.get('detail-data', {}).get('api_json', {}).get('s_count', '')
#
#             item_mongo['fa_name'] = data.get('fa_name', '')
#             item_mongo['md5'] = data.get('id', '')
#             item_mongo['url'] = data.get('url', '')
#             item_mongo['s_path'] = data.get('s_path', '')
#             item_mongo['source'] = data.get('source', '')
#             item_mongo['vers'] = data.get('version', '')
#
#             item_mongo['province'] = ''
#             item_mongo['city'] = ''
#             item_mongo['area'] = ''
#             item_mongo['title_keyword'] = ''
#             item_mongo['intention'] = ''
#             item_mongo['type'] = ''
#             item_mongo['agg_tags'] = ''
#             item_mongo['tags'] = ''
#             item_mongo['date_str'] = ''
#             item_mongo['author'] = ''
#             item_mongo['hospital'] = ''
#             item_mongo['articles_from'] = ''
#             item_mongo['v_count'] = ''
#             item_mongo['manual'] = ''
#             item_mongo['disease_site'] = ''
#             item_mongo['symptom'] = ''
#             item_mongo['cause'] = ''
#             item_mongo['t_type'] = ''
#             item_mongo['alias'] = ''
#
#             self.parse(item_mongo, fa_name)
#         except Exception as e:
#             self.write_error_text(fa_name, e, traceback.print_exc())
#
# if __name__ == '__main__':
#     version = '20210823-ddsy'
#     ddky = DdKy()
#     ddky.run(version)
#     ddky.check_mongo_data(version)
#     ddky.close()
#
#     # print(ddky.mongo.h_spider.spider_data.find_one({'version': '%s' % version}))
